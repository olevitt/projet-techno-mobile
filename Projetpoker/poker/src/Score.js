import React from "react";

const Score = (props) => {
    return (
        <div>
            Score : {props.score}
        </div>
    );
};

export default Score