export const calcScore = (main) => {
  var a = 0;
  main.map((carte) => (a = a + carte.valeur1));
  if (a <= 21) {
    return a;
  } else {
    a = 0;
    main.map((carte) => (a = a + carte.valeur2));
    return a;
  }
};
