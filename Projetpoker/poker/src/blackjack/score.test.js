import { calcScore } from "./score";

test("une main vide vaut 0", () => {
  expect(calcScore([])).toBe(0);
});
