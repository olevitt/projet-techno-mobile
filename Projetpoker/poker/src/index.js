import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import Stack from "./Stack";
import PiocherCarte from "./Cartes";
import { calcScore } from "./blackjack/score";

const Miser = ({ onMise }) => {
  return (
    <>
      <input
        type="number"
        id="mise"
        name="mise"
        min="0"
        max="100"
        placeholder="Montant de la mise"
      />

      <button onClick={() => onMise(document.getElementById("mise").value)}>
        Miser
      </button>
    </>
  );
};

const Main = ({ main }) => {
  return main.map((carte, i) => <Carte carte={carte} key={i} />);
};

const piocheBanque = (onpiochebanque, scorebanque, onscorebanque) => {
  while (scorebanque < 17) {
    onpiochebanque();
    onscorebanque();
  }
};

const EnJeu = ({ onpioche, onpiochebanque, scorebanque, onscorebanque }) => {
  return (
    <>
      <button onClick={() => onpioche()}>Piocher une carte</button>

      <button
        onClick={() => piocheBanque(onpiochebanque, scorebanque, onscorebanque)}
      >
        S'arrêter
      </button>
    </>
  );
};

const FinJeu = ({ finmanche }) => {
  return (
    <>
      <div>Vous avez fait Banqueroute !!</div>
      <button onClick={() => finmanche()}>Rejouer</button>
    </>
  );
};

const Jouer = ({
  score,
  main,
  mise,
  onscore,
  onscorebanque,
  mainb,
  onpioche,
  finmanche,
  onpiochebanque,
  scorebanque,
}) => {
  const [moments, setMoments] = React.useState("En jeu");

  const MomentJeu = ({ moment }) => {
    return (
      <>
        {moment && moment === "En jeu" ? (
          <EnJeu
            onpioche={onpioche}
            onpiochebanque={onpiochebanque}
            scorebanque={scorebanque}
            onscorebanque={onscorebanque}
          />
        ) : (
          <FinJeu finmanche={finmanche} />
        )}
      </>
    );
  };
  const Banqueroute = ({ score }) => {
    if (score > 21) {
      setMoments("finmanche");
    }
    return null;
  };
  return (
    <>
      <Banqueroute score={score} />
      <Main main={main} />
      <CarteB carteb={mainb[1]} />
      <Main main={mainb} />
      <div>{score}</div>
      <div id="mm">Montant misé :</div>
      <div id="montantmise">{mise}</div>
      <MomentJeu moment={moments} />
    </>
  );
};
const Carte = ({ carte }) => {
  return (
    <>
      <img src={carte.Image} alt="carte" width={100} className="carte" />
    </>
  );
};

const CarteB = ({ carteb }) => {
  return (
    <>
      <img src={carteb.Image} alt="carteb" width={100} className="carteb" />
    </>
  );
};
const Jeu = () => {
  const [stack, setStack] = React.useState(200);
  const [mise, setMise] = React.useState(0);
  const [main, setMain] = React.useState([PiocherCarte(), PiocherCarte()]);
  const [moment, setMoment] = React.useState("mise");
  const [mainbanque, setMainbanque] = React.useState([
    PiocherCarte(),
    PiocherCarte(),
  ]);

  const MainDeDepart = () => {
    setMain([PiocherCarte(), PiocherCarte()]);
    setMainbanque([PiocherCarte(), PiocherCarte()]);
  };
  const piocherUneCarte = () => {
    setMain([...main, PiocherCarte()]);
  };
  const piocherUneCarteBanque = () => {
    setMainbanque([...mainbanque, PiocherCarte()]);
  };

  const handleMiser = (nombre) => {
    setMise(nombre);
    setStack(stack - nombre);
    setMoment("distcarte");
  };

  const finManche = () => {
    setMoment("mise");
    MainDeDepart();
  };

  const MomentPartie = ({ moment }) => {
    return (
      <>
        {moment && moment === "mise" ? (
          <Miser onMise={handleMiser} />
        ) : (
          <Jouer
            score={calcScore(main)}
            main={main}
            mise={mise}
            onpioche={piocherUneCarte}
            onscore={calcScore}
            onscorebanque={calcScore}
            mainb={mainbanque}
            finmanche={finManche}
            onpiochebanque={piocherUneCarteBanque}
            scorebanque={calcScore(mainbanque)}
          />
        )}
      </>
    );
  };
  return (
    <>
      <img
        src="https://www.media-rdc.com/medias/7fa59b238a4a320eaba0025ba548e44e/p_580x580/table-de-poker-nevada-black-jack-unie-pliante-verte-avec-rack-de-rangement-pour-jeton.jpg"
        alt="table de pokers"
        width={900}
        className="table"
      />
      <Stack stack={stack} />
      <MomentPartie moment={moment} />
    </>
  );
};

const Menu = ({ onChange }) => {
  return (
    <ol>
      <li>
        <button onClick={() => onChange("jeu")}>Jeu</button>
      </li>
      <li>
        <button onClick={() => onChange("pas-jeu")}>Pas jeu</button>
      </li>
    </ol>
  );
};
const PasJeu = () => {
  return <div>Pas jeu</div>;
};
const ZonePrincipale = ({ ongletCourant }) => {
  return <>{ongletCourant && ongletCourant === "jeu" ? <Jeu /> : <PasJeu />}</>;
};
const Application = () => {
  const [ongletCourant, setOngletCourant] = React.useState("jeu");
  return (
    <>
      <Menu onChange={(valeur) => setOngletCourant(valeur)} />
      <ZonePrincipale ongletCourant={ongletCourant} />
    </>
  );
};
ReactDOM.render(
  <React.StrictMode>
    <Application />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
